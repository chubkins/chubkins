ssh-keygen
cat ~/.ssh/id_rsa.pub

git remote add origin https://chubkins@bitbucket.org/chubkins/chubkins.git

heroku --version
if no heroku then run: source <(curl -sL https://cdn.learnenough.com/heroku_install)
heroku login
heroku keys:add
heroku create
bundle install --without production
git push heroku master
https://chubkins-lab.herokuapp.com/

sudo service postgresql start
rails s -b $IP -p $PORT

rails generate scaffold user name email password_digest
For secured password add to gem file: 
gem 'bcrypt', '3.1.11'

update user view and delete password_digest and insert password and confirm password attributes:
user form (delete password digest) and add:
  <div class="field">
    <%= form.label :password %>
    <%= form.password_field :password %>
  </div>
  
  <div class="field">
    <%= form.label :password_confirmation, "Confirmation" %>
    <%= form.password_field :password_confirmation %>
  </div>

delete password digest from view > user > show and index

user controller, update the white listed attributes to:
    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end

Update user model to:

class User < ApplicationRecord
  before_save { email.downcase! }
  validates :name, presence: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: true
                    
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }
end

bundle
rails server and check /users and CRUD users

git push heroku
heroku run rails db:migrate

------------------------------------
Internationalise I8N

Add to gem file: 
gem 'rails-i18n'

Add to Application controller:
  before_action :set_locale

  private
  
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
    
    case I18n.locale
    when :fa, :ar
      @local_dir = "rtl"
    else
      @local_dir = "ltr"
    end
  end
  
  def default_url_options
    { locale: I18n.locale }
  end


See default YML translation files for each language here: https://github.com/svenfuchs/rails-i18n/tree/master/rails/locale
copy each desired language file to config/locales

Place routes in a locale namespace, edit config/routes
  scope "(:locale)", locale: /en|fa|ne|th/ do
    resources :users
    root 'static_pages#show'
    get ':path', to: 'static_pages#show'
    get '*path', to: 'static_pages#show', as: :pages
  end


In the view/static_pages folder, 
add a few about_us.<lang>.html.erb files (where <lang> is the country code, such as en fa ne and so on)

Insert the following in each file for testing:
<h1><%=t :hello %></h1>
<p><%= flash[:notice] %></p>
<p><%= l Time.now, format: :short %></p>


Add the following to the static_pages show method
    flash[:notice] = t(:hello)
    

To choose a language, add the following to the layout/application.html.erb

  <nav>
    <div class="menu" lang = <%= I18n.locale %> dir= <%= @local_dir %> >
      <ul class="vertical menu">
        <li><%= t('languages.languages') %>:
            <%= link_to t('languages.en'), root_path(locale: 'en'), html_options = { dir: "ltr", lang: "en" } %> 
            <%= link_to t('languages.fa'), root_path(locale: 'fa'), html_options = { dir: "rtl", lang: "fa" } %>
            <%= link_to t('languages.ne'), root_path(locale: 'ne'), html_options = { dir: "rtl", lang: "ne" } %>
            <%= link_to t('languages.th'), root_path(locale: 'th'), html_options = { dir: "rtl", lang: "th" } %>
            <hr />
        </li>
        <li><%= link_to t('about_us'), pages_path('about_us') %></li>
      </ul>
    </div>
  </nav>
  
---- globalize

udate gem file:
gem 'globalize', git: 'https://github.com/globalize/globalize'
gem 'activemodel-serializers-xml'
bundle

class User < ApplicationRecord
  translates :name
  
update locale yml with db attributes

rails g migration create_user_translations

class CreateUserTranslations < ActiveRecord::Migration[5.1]
  def up
    User.create_translation_table!(
      { name: :string},
      { migrate_data: true }
    )
  end

  def down
    User.drop_translation_table! migrate_data: true
  end
end

rails db:migrate

-------------------------
Hartl Ch 8

rails generate controller Sessions new

  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'

Top labs model

rails generate scaffold topic name description
rails generate scaffold lab name summary sheet:text
rails generate scaffold topic_lab lab_order:integer topic_id:integer lab_id:integer
rails generate scaffold rating value topic_lab_id:integer

