class CreateTopicLabs < ActiveRecord::Migration[5.1]
  def change
    create_table :topic_labs do |t|
      t.integer :lab_order
      t.integer :topic_id
      t.integer :lab_id

      t.timestamps
    end
  end
end
