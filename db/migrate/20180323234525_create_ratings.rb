class CreateRatings < ActiveRecord::Migration[5.1]
  def change
    create_table :ratings do |t|
      t.string :value
      t.integer :topic_lab_id

      t.timestamps
    end
  end
end
