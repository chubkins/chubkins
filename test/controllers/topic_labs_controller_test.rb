require 'test_helper'

class TopicLabsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @topic_lab = topic_labs(:one)
  end

  test "should get index" do
    get topic_labs_url
    assert_response :success
  end

  test "should get new" do
    get new_topic_lab_url
    assert_response :success
  end

  test "should create topic_lab" do
    assert_difference('TopicLab.count') do
      post topic_labs_url, params: { topic_lab: { lab_id: @topic_lab.lab_id, lab_order: @topic_lab.lab_order, topic_id: @topic_lab.topic_id } }
    end

    assert_redirected_to topic_lab_url(TopicLab.last)
  end

  test "should show topic_lab" do
    get topic_lab_url(@topic_lab)
    assert_response :success
  end

  test "should get edit" do
    get edit_topic_lab_url(@topic_lab)
    assert_response :success
  end

  test "should update topic_lab" do
    patch topic_lab_url(@topic_lab), params: { topic_lab: { lab_id: @topic_lab.lab_id, lab_order: @topic_lab.lab_order, topic_id: @topic_lab.topic_id } }
    assert_redirected_to topic_lab_url(@topic_lab)
  end

  test "should destroy topic_lab" do
    assert_difference('TopicLab.count', -1) do
      delete topic_lab_url(@topic_lab)
    end

    assert_redirected_to topic_labs_url
  end
end
