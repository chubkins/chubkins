class ApplicationController < ActionController::Base
  
  protect_from_forgery with: :exception
  include SessionsHelper

  before_action :set_locale
  
  private
  
  def set_locale
    pp params
    I18n.locale = params[:locale] || I18n.default_locale
    
    case I18n.locale
    when :fa, :ar
      @local_dir = "rtl"
    else
      @local_dir = "ltr"
    end
  end
  
  def default_url_options
    { locale: I18n.locale }
  end

  def admin_user
    redirect_to(root_url) unless !current_user.nil? and current_user.email == 'ebbi@example.com' 
  end

end