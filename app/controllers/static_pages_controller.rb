class StaticPagesController < ApplicationController
  def show
    
    if params[:pages].present?
      file_path = 'app/views/static_pages/' + params[:pages] + '.html.erb'
      path = 'static_pages/' + params[:pages] if File.exists? Rails.root.join(file_path)
    else
      path = 'show'
    end
    
    render path

  end 
end
