class TopicLabsController < ApplicationController
  before_action :set_topic_lab, only: [:show, :edit, :update, :destroy]
  before_action :admin_user, except: [:show]

  # switch between static and db lab sheet
  @db_lab_sheet = true;

  # GET /topic_labs
  # GET /topic_labs.json
  def index
    @topic_labs = TopicLab.all
  end

  # GET /topic_labs/1
  # GET /topic_labs/1.json
  def show

    @topics = Topic.all.order(name: :asc)
    @prev_topic_lab = TopicLab.prev_topic_lab(@topic_lab.id)
    @next_topic_lab = TopicLab.next_topic_lab(@topic_lab.id)
    
    @rating = Rating.new
    @total_votes = Rating.total_votes(@topic_lab)
    @average_rating = Rating.average_value(@topic_lab)
    
    @progress = (@topic_lab.lab_order.to_f / TopicLab.no_of_labs(@topic_lab.topic_id)) * 100
 
  end

  # GET /topic_labs/new
  def new
    @topic_lab = TopicLab.new
  end

  # GET /topic_labs/1/edit
  def edit
  end

  # POST /topic_labs
  # POST /topic_labs.json
  def create
    @topic_lab = TopicLab.new(topic_lab_params)

    respond_to do |format|
      if @topic_lab.save
        format.html { redirect_to @topic_lab, success: 'Topic lab was successfully created.' }
        format.json { render :show, status: :created, location: @topic_lab }
      else
        format.html { render :new }
        format.json { render json: @topic_lab.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /topic_labs/1
  # PATCH/PUT /topic_labs/1.json
  def update
    respond_to do |format|
      if @topic_lab.update(topic_lab_params)
        format.html { redirect_to @topic_lab, success: 'Topic lab was successfully updated.' }
        format.json { render :show, status: :ok, location: @topic_lab }
      else
        format.html { render :edit }
        format.json { render json: @topic_lab.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /topic_labs/1
  # DELETE /topic_labs/1.json
  def destroy
    @topic_lab.destroy
    respond_to do |format|
      format.html { redirect_to topic_labs_url, success: 'Topic lab was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_topic_lab
      @topic_lab = TopicLab.find(params[:id])
      @lab_sheet = Lab.find(@topic_lab.lab_id).sheet
      
      @db_lab_sheet ?
        @lab_sheet = Lab.find(@topic_lab.lab_id).sheet : 
        @lab_sheet = "/labs/sheets/" + Topic.find(@topic_lab.topic_id).name.downcase + '/' + Lab.find(@topic_lab.lab_id).name
    
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def topic_lab_params
      params.require(:topic_lab).permit(:lab_order, :topic_id, :lab_id)
    end
end
