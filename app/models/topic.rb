class Topic < ApplicationRecord
    has_many :topic_labs, -> { order(lab_order: :asc) }
    has_many :labs, through: :topic_labs
end
