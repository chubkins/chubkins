class Lab < ApplicationRecord
  has_many :topic_labs
  has_many :topics, through: :topic_labs
end
