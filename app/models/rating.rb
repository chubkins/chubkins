class Rating < ApplicationRecord
  
  def self.total_votes(topic_lab_id)
    self.where(topic_lab_id: topic_lab_id).count
  end

  def self.average_value(topic_lab_id)
    self.where(topic_lab_id: topic_lab_id).average(:value)
  end
    
end
