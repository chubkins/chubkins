class TopicLab < ApplicationRecord
  
  belongs_to :topic
  belongs_to :lab
  has_many :rating
  
  private 
  
  def self.no_of_labs(topic_id)
    return self.where(topic_id: topic_id).count
  end  

  def self.next_topic_lab(topic_lab_id)
    topic_lab = self.find(topic_lab_id)
    next_lab = self.find_by(lab_order: topic_lab.lab_order + 1)
    
    if next_lab.nil? 
      return topic_lab
    else
      return next_lab
    end
  end

  def self.prev_topic_lab(topic_lab_id)
    topic_lab = self.find(topic_lab_id)
    prev_lab = self.find_by(lab_order: topic_lab.lab_order - 1)
    
    if prev_lab.nil? 
      return topic_lab
    else
      return prev_lab
    end
  end
  
end
