json.extract! rating, :id, :value, :topic_lab_id, :created_at, :updated_at
json.url rating_url(rating, format: :json)
