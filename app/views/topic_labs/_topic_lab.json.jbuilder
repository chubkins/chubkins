json.extract! topic_lab, :id, :lab_order, :topic_id, :lab_id, :created_at, :updated_at
json.url topic_lab_url(topic_lab, format: :json)
