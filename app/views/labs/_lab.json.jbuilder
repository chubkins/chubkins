json.extract! lab, :id, :name, :summary, :sheet, :created_at, :updated_at
json.url lab_url(lab, format: :json)
