The lab sheets are retrieved from the DB.

Labs > edit

Copy lab sheet content into corresponding file here for editing and copying back to store in DB
(smaller edits in the lab edit form; larger edits, the color coding is useful)