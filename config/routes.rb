Rails.application.routes.draw do

  scope "(:locale)" do
    get    '/login',   to: 'sessions#new'
    post   '/login',   to: 'sessions#create'
    delete '/logout',  to: 'sessions#destroy'
    resources :users
    resources :topics
    resources :labs
    resources :topic_labs do
      resources :ratings
    end
    
    root 'static_pages#show'
    get ':pages', to: 'static_pages#show', as: :pages
    get '*pages', to: 'static_pages#show'
    
    
  end

end
